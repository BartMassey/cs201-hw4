/* Copyright © 2018 Bart Massey */

#include <stdio.h>
#include "bits.h"

int
main(int argc, char **argv) {
    struct bits *bitmap = asciitobits(stdin, '+');
    if (bitmap == 0) {
        perror("asciitobits");
        return 1;
    }
    bitstoascii(stdout, 'o', ' ', bitmap);
    return 0;
}
