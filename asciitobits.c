/* Copyright © 2018 Bart Massey */

#include <stdlib.h>
#include "bits.h"

struct bits *
asciitobits(FILE *f, char black_char) {
    struct bits *result = malloc(sizeof *result);
    if (result == 0)
        return 0;
    result->nrows = 0;
    result->ncols = 0;
    result->bits = malloc(sizeof *result->bits);
    if (result->bits == 0)
        return 0;
    size_t capacity = 32;
    size_t bits_read = 0;
    int ncols = 0;
    int ch;
    while ((ch = fgetc(f)) != EOF) {
        if (ch == '\n') {
            if (result->nrows == 0)
                result->ncols = ncols;
            if (result->ncols != ncols) {
                free(result->bits);
                free(result);
                return 0;
            }
            result->nrows++;
            ncols = 0;
            continue;
        }
        if (bits_read >= capacity) {
            capacity *= 2;
            result->bits = realloc(result->bits, capacity);
            if (result->bits == 0)
                return 0;
        }
        if (ch == black_char) {
            result->bits[bits_read >> 5] |= 1 << (bits_read & 0x1f);
        } else {
            result->bits[bits_read >> 5] &= ~(1 << (bits_read & 0x1f));
        }
        bits_read++;
        ncols++;
    }
    if (ncols > 0) {
        if (result->nrows == 0)
            result->ncols = ncols;
        if (ncols != result->ncols) {
            free(result->bits);
            free(result);
            return 0;
        }
        result->nrows++;
    }
    return result;
}
