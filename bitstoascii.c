/* Copyright © 2018 Bart Massey */

#include <stdlib.h>
#include "bits.h"

void
bitstoascii(FILE *f, char black_char, char white_char, struct bits *bitmap) {
    size_t posn = 0;
    for (int r = 0; r < bitmap->nrows; r++) {
        for (int c = 0; c < bitmap->ncols; c++) {
            int bit = (bitmap->bits[posn >> 5] >> (posn & 0x1f)) & 1;
            if (bit == 0)
                fputc(white_char, f);
            else
                fputc(black_char, f);
            posn++;
        }
        fputc('\n', f);
    }
}
