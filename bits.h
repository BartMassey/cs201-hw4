/* Copyright © 2018 Bart Massey */

#include <stdio.h>
#include <stdint.h>

struct bits {
    int nrows, ncols;
    uint32_t *bits;
};

extern struct bits *
asciitobits(FILE *f, char black_char);

extern void
bitstoascii(FILE *f, char black_char, char white_char, struct bits *bitmap);
