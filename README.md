# ASCII Bitmap Manipulation
Copyright (c) 2018 Bart Massey

This code is intended for the use of Bart Massey's Portland
State University Winter 2018 CS 201 class.

The code currently comprises:

* A static library of routines for reading and writing
  ASCII-text binary images, converting them to and from an
  internal bitmap format.

* A driver program that reads an ASCII image from standard
  input into a bitmap, and writes the bitmap as an ASCII
  image to standard output.

* A sample ASCII image.

Try this code out by typing "make" and then

        ./convbits <smiley.txt

## Homework Assignment

Document your work on all problems below in a file named
"WRITEUP.txt". Start this file with your name and PSU email
address.

  1. *Manual Reading:* The Makefile given here uses the `ar`
     command in an old style to create the static library.
     Read the Linux `ar` manpage ("man ar" at the command
     line) and modify the arguments to `ar` in the Makefile to
     make the `ranlib` line in the Makefile unnecessary.

  2. *Code Reading:* Add comments above `asciitobits()` and
     `bitstoascii()` that describe in detail what these
     functions do, what their return values are, and under
     what conditions they may fail.

  3. *Repair:* Right now, `asciitobits()` will likely return a
     bitmap that is bigger than needed. Modify this function
     (with appropriate comments) to return a bitmap that has
     at most 32 extra bits.

  4. *Completion:* Add a new library file `freebits.c` with
     a function `freebits()` that will free both the `struct
     bits` and the bitmap itself. Don't forget to alter
     `bits.h` and the `Makefile`. Alter the `convbits`
     program to call `freebits()` when appropriate.

  5. *Command-Line Programming:* Modify `convbits.c` to take
     optional command-line arguments:
       * `-i` *filename*: Instead of reading from standard
         input, read from the file *filename*.
       * `-o` *filename*: Instead of writing to standard output,
         write to the file *filename*.

     You may either use the `getopt` library (say "man 3
     getopt") or write your own code for argument handling.
     The second thing will probably be easier.  You may want
     to look at the manual page for `fopen()` (say "man 3
     fopen").

  6. **Extra Credit, not required:** Add a new library file `life.c`
     with a function `life()` that takes one step in Conway's
     "[Game of Life](http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)"
     on the given bitmap. Modify `convbits.c` to take an
     argument `-l` *nsteps* to make `convbits` print *nsteps*
     ASCII generations of a life game starting with the
     input image on its output.
